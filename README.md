Start server:

```sh
npx parcel frontend/index.html
```

Run tests:

```sh
npx cypress run
```
