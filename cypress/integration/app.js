describe("Simple List", function() {
    beforeEach(function() {
        cy.visit("http://localhost:1234");
    })

    it("shows the right header", function() {
        cy.contains("This is a list with 1 items:");
    });

    it("adds an item", function() {
        cy.get("input[type=text]").type("This is a test!");
        cy.contains("Add!").click();
        cy.contains("This is a list with 2 items:");
    });

});