import gg from "glasgow";
// @jsx gg

let list = ["test"];

class Item {
    render() {
        return <section>{this.name}</section>;
    }
}

class Main {
    render() {
        return <main>
            <h1>This is a list with {list.length} items:</h1>
            <input type="text" placeholder="Your item..." binding="$text" />
            <input type="submit" value="Add!" onclick={this.addItem} /> 
            {list.map(name => <Item name={name} />)}
        </main>;
    }
    addItem() {
        list.push(this.$text);
        this.$text = "";
    }
}

gg.mount(document.body, Main);
